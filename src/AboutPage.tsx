import React from 'react';
import { LinkButton } from './LinkButton';

export function AboutPage(): JSX.Element {
    return <div>
		<LinkButton to='/'>
			Wróć
		</LinkButton>
		This is my wunderbar hello world page!
	</div>;
}
