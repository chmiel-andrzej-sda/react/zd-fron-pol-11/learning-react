import React from 'react';
import { Navigate, useParams } from 'react-router-dom';
import { PersonAndAddress } from './entity/PersonAndAddress';
import { LinkButton } from './LinkButton';
import { PersonalAndAddressData } from './PersonalAndAddressData';

interface PersonComponentProps {
	readonly people: PersonAndAddress[];
}

export function PersonComponent(props: PersonComponentProps): JSX.Element {
	const params = useParams();

	const index: number = +(params.index || '');

	if (isNaN(index) || index < 0 || index >= props.people.length) {
		return <Navigate to='/people'/>;
	}

	const person: PersonAndAddress = props.people[index];

	return <div>
		<LinkButton to='/people'>
			Wróć
		</LinkButton>
		<PersonalAndAddressData personAndAddress={person}/>
	</div>;
}
