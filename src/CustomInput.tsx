import React, { HTMLInputTypeAttribute } from 'react';

interface CustomInputProps {
    readonly label: string;
    readonly required?: boolean;
    readonly type?: HTMLInputTypeAttribute;
    readonly value: string;
    readonly onChange: (value: string) => void;
}

export function CustomInput(props: CustomInputProps): JSX.Element {
    function checkRequired(): JSX.Element {
        if (props.required) {
            return <span style={{color: 'red'}}>*</span>
        }
        return <></>;
    }

    function handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
        props.onChange(event.target.value);
    }

    return <div>
        {props.label} {checkRequired()} <input type={props.type} value={props.value} onChange={handleChange}/>
    </div>;
}
