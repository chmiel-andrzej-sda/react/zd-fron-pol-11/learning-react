import React from 'react';
import { CustomComponent } from './CustomComponent';
import { PersonAndAddress } from './entity/PersonAndAddress';
import { MyButton } from './MyButton';
import { MyComponent } from './MyComponent';
import { UserDataForm } from './UserDataForm';
import { Image } from "./Image";
import { PersonalAndAddressData } from './PersonalAndAddressData';

interface MainPageProps {
    readonly people: PersonAndAddress[];
    readonly onCreate: (personAndAddress: PersonAndAddress) => void;
}

export function MainPage(props: MainPageProps): JSX.Element {
	const [count, setCount] = React.useState<number>(0);
	const reverse: boolean = false;

    function handleClick(): void {
		if (reverse) {
			setCount((prev: number): number => prev - 1);
		} else {
			setCount((prev: number): number => prev + 1);
		}
	}

	function renderPersonAndAddress(personAndAddress: PersonAndAddress, index: number): JSX.Element {
		return <PersonalAndAddressData personAndAddress={personAndAddress} key={index}/>;
	}

    return <div>
        <MyComponent name="Andrzej"/>
		<MyComponent name="Tomek"/>
		<MyComponent name="Piotrek"/>
		<MyComponent/>
		<Image
			src="https://www.vets4pets.com/siteassets/species/cat/kitten/tiny-kitten-in-sunlight.jpg"
			alt="Kicia"
		/>
		{props.people
			.sort((p1: PersonAndAddress, p2: PersonAndAddress): number =>
					p1.person.lastName.localeCompare(p2.person.lastName))
			.map(renderPersonAndAddress)}
		<MyButton
			value={count === 0 ? "default" : `${count}`}
			onClick={handleClick}
		/>
		{count < 10 && <CustomComponent/>}
		<UserDataForm
			onCreate={props.onCreate}
		/>
    </div>;
}
