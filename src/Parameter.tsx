import React from 'react';
import { useParams } from 'react-router-dom';

export function Parameter(): JSX.Element {
    type NumberParams = { a?: string; b?: string; };
    const params: NumberParams = useParams<NumberParams>();

    console.log(params);

    const x = +(params.a || '');
    const y = +(params.b || '');

    if (isNaN(x) || isNaN(y)) {
        return <div>Not a Number</div>;
    }

    return <div>{x + y}</div>;
}
