import React from 'react';
import { PersonAndAddress } from './entity/PersonAndAddress';
import { LinkButton } from './LinkButton';

interface PersonListComponentProps {
	readonly people: PersonAndAddress[];
}

export function PersonListComponent(props: PersonListComponentProps): JSX.Element {
	function renderRow(person: PersonAndAddress, index: number): JSX.Element {
		return <tr key={index}>
			<td>{person.person.firstName}</td>
			<td>{person.person.lastName}</td>
			<td>{person.person.pesel}</td>
			<td><LinkButton to={`/people/${index}`}>Szczegóły</LinkButton></td>
		</tr>;
	}

    return <div>
		<LinkButton to='/'>
			Wróć
		</LinkButton>
		<table border={1}>
			<thead>
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>PESEL</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				{props.people.map(renderRow)}
			</tbody>
		</table>
	</div>;
}
