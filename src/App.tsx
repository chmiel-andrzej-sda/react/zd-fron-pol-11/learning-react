import React from 'react';
import { Link } from 'react-router-dom';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { AboutPage } from './AboutPage';
import { PersonAndAddress } from './entity/PersonAndAddress';
import { MainPage } from './MainPage';
import { MyComponent } from './MyComponent';
import { Parameter } from './Parameter';
import { PersonComponent } from './PersonComponent';
import { PersonListComponent } from './PersonListComponent';

export function App(): JSX.Element {
	const [people, setPeople] = React.useState<PersonAndAddress[]>([
		{
			person: {
				firstName: "Andrzej",
				lastName: "Chmiel",
				pesel: 1234
			},
			address: {
				street: "Kolorowa 1/2",
				city: "Krakow",
				country: "PL"
			}
		},
		{
			person: {
				firstName: "Tomasz",
				lastName: "Abc",
				pesel: 4321
			},
			address: {
				street: "Czarno-biała 2/1",
				city: "Warszawa",
				country: "PL"
			}
		}
	]);

	function handleCreate(personAndAddress: PersonAndAddress): void {
		setPeople([...people, personAndAddress]);
	}

	return <div className="App">
		<BrowserRouter>
			{/*
			<MainPage
				people={people}
				onCreate={handleCreate}
			/>
			<Link to='/test'>Test</Link>
			<Link to='/value'>Value</Link>
			*/}
			<Link to='/'>Main</Link>
			<Link to='/people'>People</Link>
			<Link to='/about'>About</Link>
			<Routes>
				<Route path='/test' element={<MyComponent name='test'/>}/>
				<Route path='/value' element={<MyComponent name='value'/>}/>
				<Route path='/value/:a/:b' element={<Parameter/>}/>
				<Route path='/' element={<MyComponent name='unknown'/>}/>
				<Route path='/people' element={<PersonListComponent people={people}/>}/>
				<Route path='/people/:index' element={<PersonComponent people={people}/>}/>
				<Route path='/about' element={<AboutPage/>}/>
			</Routes>
		</BrowserRouter>
	</div>;
}