import React from 'react';

interface ImageProps {
	readonly src: string;
    readonly alt: string;
}

export function Image(props: ImageProps): JSX.Element {
	return <img src={props.src} alt={props.alt} width={480} height={640}/>;
}
