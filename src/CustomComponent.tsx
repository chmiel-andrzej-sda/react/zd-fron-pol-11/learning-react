import React from 'react';

export function CustomComponent(): JSX.Element {
    const [count, setCount] = React.useState<number>(0);

    React.useEffect(() => {
        console.log("renders");

        return () => console.log("unmount");
    }, []);

    React.useEffect((): void => {
        console.log("updates");
    }, [count]);

    function handleClick(): void {
		setCount((prev: number): number => prev + 1);
    }

    return <div onClick={handleClick}>CustomComponent</div>;
}
