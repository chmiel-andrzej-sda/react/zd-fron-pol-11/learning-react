import React from 'react'
import { Person } from './entity/Person';

export interface PersonalDetailsProps {
	readonly person: Person;
}

export function PersonalDetails(props: PersonalDetailsProps): JSX.Element {
	return <table border={1}>
		<thead>
			<tr>
				<th>First name</th>
				<th>Last name</th>
				<th>PESEL</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{props.person.firstName}</td>
				<td>{props.person.lastName}</td>
				<td>{props.person.pesel}</td>
			</tr>
		</tbody>
	</table>;
}
