import { Address } from "./Address";
import { Person } from "./Person";

export interface PersonAndAddress {
    readonly person: Person;
    readonly address: Address;
}
