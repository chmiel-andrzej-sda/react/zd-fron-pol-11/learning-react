export interface Person {
	readonly firstName: string;
	readonly lastName: string;
	readonly pesel: number;
}
