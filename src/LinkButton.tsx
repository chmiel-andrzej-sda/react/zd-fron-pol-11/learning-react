import React from 'react';
import { NavigateFunction, useNavigate } from 'react-router-dom';

interface LinkButtonProps {
	readonly to: string;
}

export function LinkButton(props: React.PropsWithChildren<LinkButtonProps>): JSX.Element {
	const navigate: NavigateFunction = useNavigate();

	function handleClick(): void {
		navigate(props.to);
	}

	return <div>
		<button onClick={handleClick}>{props.children}</button>
	</div>;
}
