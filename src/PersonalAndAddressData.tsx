import React from 'react';
import { AddressData } from './AddressData';
import { PersonAndAddress } from './entity/PersonAndAddress';
import { PersonalDetails } from './PersonalDetails';

export interface PersonalAndAddressDataProps {
    readonly personAndAddress: PersonAndAddress;
}

export function PersonalAndAddressData(props: PersonalAndAddressDataProps): JSX.Element {
	return <div>
		<PersonalDetails person={props.personAndAddress.person}/>
		<AddressData address={props.personAndAddress.address}/>
	</div>;
}
