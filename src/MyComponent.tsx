import React from 'react';
import { Greeting } from "./Greeting";

interface MyComponentProps {
    readonly name?: string;
}

export function MyComponent(props: MyComponentProps): JSX.Element {
    return <div style={{color: 'red'}}>
        {props.name && <Greeting name={props.name}/>}
    </div>;
}