import React from 'react'
import { CustomInput } from './CustomInput';
import { PersonAndAddress } from './entity/PersonAndAddress';

interface UserDataFormProps {
	onCreate: (personAndAddress: PersonAndAddress) => void;
}

export function UserDataForm(props: UserDataFormProps): JSX.Element {
	const [gender, setGender] = React.useState<string>('M');
	const [firstName, setFirstName] = React.useState<string>('');
	const [lastName, setLastName] = React.useState<string>('');
	const [pesel, setPesel] = React.useState<string>('');
	const [street, setStreet] = React.useState<string>('');
	const [city, setCity] = React.useState<string>('');
	const [country, setCountry] = React.useState<string>('');

	function handleGenderChange(event: React.ChangeEvent<HTMLSelectElement>) {
		setGender(event.target.value);
	}

	function getGenderTextInGenitive(): string {
		if (gender === 'K') {
			return 'Pani';
		}
		return 'Pana';
	}

	function handleSubmit(event: React.FormEvent<HTMLFormElement>): void {
		event.preventDefault();
		if (firstName === '' || lastName === '' || pesel === '') {
			alert("Uzupełnij!");
			return;
		}
		props.onCreate({
			person: {
				firstName,
				lastName,
				pesel: +pesel
			},
			address: {
				street,
				city,
				country
			}
		});
	}

    return <form onSubmit={handleSubmit}>
		Płeć: 
		<select onChange={handleGenderChange}>
			<option>M</option>
			<option>K</option>
		</select><br/>
		<CustomInput 
			label={`Jakie jest ${getGenderTextInGenitive()} imię?`}
			required
			value={firstName}
			onChange={setFirstName}
		/>
		<CustomInput 
			label={`Jakie jest ${getGenderTextInGenitive()} nazwisko?`}
			required
			value={lastName}
			onChange={setLastName}
		/>
		<CustomInput 
			label={`Jaki jest ${getGenderTextInGenitive()} PESEL?`}
			type='number'
			required
			value={pesel}
			onChange={setPesel}
		/>
		<CustomInput 
			label={`Jaka jest ${getGenderTextInGenitive()} ulica?`}
			value={street}
			onChange={setStreet}
		/>
		<CustomInput 
			label={`Jakie jest ${getGenderTextInGenitive()} miasto?`}
			value={city}
			onChange={setCity}
		/>
		<CustomInput 
			label={`Jaki jest ${getGenderTextInGenitive()} kraj?`}
			value={country}
			onChange={setCountry}
		/>
		<button type='submit'>WYŚLIJ</button>
	</form>;
}
