import React from 'react';

interface MyButtonProps {
	readonly value: string;
	readonly onClick: () => void;
}

export function MyButton(props: MyButtonProps): JSX.Element {
	return <button onClick={props.onClick}>
		{props.value}
	</button>
}
