import React from 'react';
import { Address } from './entity/Address';

export interface AddressDataProps {
	readonly address: Address;
}

export function AddressData(props: AddressDataProps): JSX.Element {
	return <table border={1}>
		<thead>
			<tr>
				<th>Ulica</th>
				<th>Miasto</th>
				<th>Kraj</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{props.address.street}</td>
				<td>{props.address.city}</td>
				<td>{props.address.country}</td>
			</tr>
		</tbody>
	</table>;
}
