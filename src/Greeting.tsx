import React from 'react';

interface GreetingProps {
    readonly name: string;
}

export function Greeting(props: GreetingProps): JSX.Element {
    return <p>Hello there, {props.name}!</p>
}